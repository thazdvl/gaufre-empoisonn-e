package projet.gaufre;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import Dessin.ObjetGraphique;
import Dessin.Fenetre;

class TerrainGraphique implements ObjetGraphique, Observer {
    Fenetre f;
    Terrain t;
    BufferedImage libre, j1, j2, poison;
    
    private Color [][] statut;
    
    public Color getStatut(int I,int J) {
        return statut[I][J];
    }
    
    public void setStatut(Color i, int I, int J) {
        statut[I][J]=i;
    }

    
    private URL getImage(String nom) {
        ClassLoader cl = getClass().getClassLoader();
        return cl.getResource("Images/" + nom);
    }

    TerrainGraphique(Fenetre f, Terrain t) {
        this.f = f;
        this.t = t;
        statut = new Color [t.hauteur()][t.largeur()];
        
        for (int i=0; i<t.hauteur(); i++) {
            for (int j=0; j<t.largeur(); j++) {
                statut[i][j]=Color.white;
            }
        }
        libre = new BufferedImage(largeurCase(), hauteurCase(), BufferedImage.TYPE_INT_RGB);
        j1 = new BufferedImage(largeurCase(), hauteurCase(), BufferedImage.TYPE_INT_RGB);
        j2 = new BufferedImage(largeurCase(), hauteurCase(), BufferedImage.TYPE_INT_RGB);
        poison = new BufferedImage(largeurCase(), hauteurCase(), BufferedImage.TYPE_INT_RGB);
        
        remplirCase(Color.white, libre);
        remplirCase(Color.blue, j1);
        remplirCase(Color.red, j2);
        remplirCase(Color.green, poison);
    }
    
    public void remplirCase(Color c, BufferedImage buff) {
        Graphics2D g = buff.createGraphics();
        
        g.setPaint(c);
        g.fillRect(0, 0, largeurCase(), hauteurCase());
        
        g.setPaint(Color.black);
        g.drawRect(0, 0, largeurCase(), hauteurCase());
    }

    public ObjetGraphique clone() {
        TerrainGraphique nt = new TerrainGraphique(f, t.clone());
        for (int i=0; i<t.hauteur(); i++) {
            for (int j=0; j<t.largeur(); j++) {
                nt.statut[i][j]=statut[i][j];
            }
        }
        
        return nt;

    }

    public int largeurCase() {
        return f.largeur() / t.largeur();
    }

    public int hauteurCase() {
        return f.hauteur() / t.hauteur();
    }

    public void draw(Graphics2D g) {
        // On efface tout
        g.setPaint(Color.white);
        g.fillRect(0, 0, f.largeur(), f.hauteur());

        int lC = largeurCase();
        int hC = hauteurCase();

        // On affiche les cases
        for (int i=0; i<t.hauteur(); i++)
            for (int j=0; j<t.largeur(); j++) {
                int x,y;
                x = j*lC;
                y = i*hC;

                switch (t.consulter(i, j)) {
                case LIBRE:
                    g.drawImage(libre, x, y, lC, hC, null);
                    break;
                case EMPOISONNE:
                    g.drawImage(poison, x, y, lC, hC, null);
                    break;
                case MANGE_J1:
                    g.drawImage(j1, x, y, lC, hC, null);
                    break;
                case MANGE_J2:
                    g.drawImage(j2, x, y, lC, hC, null);
                    break;
                }
            }
    }

    public int calculeLigne(int y) {
        return y / hauteurCase();
    }

    public int calculeColonne(int x) {
        return x / largeurCase();
    }

    public boolean equals(ObjetGraphique o) {
        TerrainGraphique autre = null;
        try {
            autre = (TerrainGraphique) o;
        } catch (ClassCastException e) {
            return false;
        }
        return t.equals(autre.t) && statut.equals(autre.statut);
    }

    public int key() {
        return 0;
    }

    public String toString() {
        return t.toString();
    }

    @Override
    public void update(Observable o, Object arg) {
        f.tracerSansDelai(this);
    }
}
