package projet.gaufre;

import java.util.LinkedList;
import java.util.Random;

import org.w3c.dom.ls.LSInput;

import Dessin.Point;

public class NiveauUn implements Joueur {
    private Terrain terrain;

    public NiveauUn(Terrain t) {
        terrain = t;
    }

    @Override
    public Point jouerCoup() {
        Random random = new Random();
        
        LinkedList<Point> pointsLibres = new LinkedList<Point>();
        for (int i=0 ; i<terrain.hauteur() ; i++)
            for (int j=0 ; j<terrain.largeur() && terrain.estMangeable(j, i); j++)
                if (terrain.estMangeable(j, i))
                    pointsLibres.add(new Point(j, i));
        
        return pointsLibres.get(random.nextInt(pointsLibres.size()));
        }
    }