package projet.gaufre;

import java.util.LinkedList;
import java.util.Observable;
import java.util.Scanner;
import java.io.InputStream;

import Dessin.Point;

public class Terrain extends Observable {
    private Case[][] terrain;
    private int      largeur, hauteur;
    
    private Terrain() {
    }

    public Terrain(InputStream s) {
        lecture(new Scanner(s));
    }

    public Terrain clone() {
        Terrain nouveau = new Terrain();
        nouveau.terrain = new Case[hauteur()][largeur()];
        nouveau.largeur = largeur();
        nouveau.hauteur = hauteur();
        nouveau.copie(terrain);
        return nouveau;
    }

    public static Terrain defaut() {
        Terrain nouveau = new Terrain();
        nouveau.terrain = new Case[8][24];
        nouveau.efface();
        nouveau.terrain[0][0] = Case.EMPOISONNE;
        nouveau.largeur = 24;
        nouveau.hauteur = 8;
        return nouveau;
    }

    public int largeur() {
        return largeur;
    }

    public int hauteur() {
        return hauteur;
    }

    public Case consulter(int ligne, int colonne) {
        return terrain[ligne][colonne];
    }

    /**
     * indique si la case est jouable : c'est à dire que la case n'est pas dans une zone non jouable ( = mangé par un
     * des joueurs). La case peut par contre être empoisonée.
     * 
     * @param x
     *        coordonnée x de la case
     * @param y
     *        coordonnée y de la case
     * @return true si la case est mangeable
     */
    public boolean estMangeable(int x, int y) {
        return terrain[y][x] != Case.MANGE_J1 && terrain[y][x] != Case.MANGE_J2;
    }

    public boolean estEmpoisonee(int x, int y) {
        return terrain[y][x] == Case.EMPOISONNE;
    }

    public Point getCaseEmpoisonnee() {
        return new Point(0, 0);
    }

    public void assigner(Case c, int ligne, int colonne) {
        terrain[ligne][colonne] = c;
    }

    public void assignerZone(Case etat, Point p) {
        for (int i = p.x; i < largeur(); i++) {
            for (int j = p.y; j < hauteur(); j++) {
                if (estMangeable(i, j)) {
                    assigner(etat, j, i);
                }
            }
        }
        setChanged();
        notifyObservers();
    }

    public void lecture(Scanner s) {
        largeur = s.nextInt();
        hauteur = s.nextInt();
        terrain = new Case[hauteur][largeur];

        for (int i = 0; i < hauteur(); i++) {
            String ligne;
            ligne = s.next();
            for (int j = 0; j < largeur(); j++)
                switch (ligne.charAt(j)) {
                    case 'x':
                        terrain[i][j] = Case.EMPOISONNE;
                        break;
                    case '.':
                        terrain[i][j] = Case.LIBRE;
                        break;
                    case '1':
                        terrain[i][j] = Case.MANGE_J1;
                        break;
                    case '2':
                        terrain[i][j] = Case.MANGE_J2;
                        break;
                    default:
                        terrain[i][j] = Case.LIBRE;
                }
        }
    }

    public String toString() {
        String resultat = largeur() + "\n" + hauteur();
        for (int i = 0; i < hauteur(); i++) {
            resultat += "\n";
            for (int j = 0; j < largeur(); j++)
                switch (terrain[i][j]) {
                    case LIBRE:
                        resultat += ".";
                        break;
                    case MANGE_J1:
                        resultat += "1";
                        break;
                    case MANGE_J2:
                        resultat += "2";
                        break;
                    case EMPOISONNE:
                        resultat += "x";
                        break;
                    default:
                        resultat += "?";
                }
        }
        return resultat;
    }

    public void efface() {
        for (int i = 0; i < terrain.length; i++)
            for (int j = 0; j < terrain[0].length; j++)
                terrain[i][j] = Case.LIBRE;
        terrain[0][0] = Case.EMPOISONNE;

        setChanged();
        notifyObservers();
    }

    private void copie(Case[][] t) {
        for (int i = 0; i < t.length; i++)
            for (int j = 0; j < t[0].length; j++)
                terrain[i][j] = t[i][j];
    }

    public boolean equals(Terrain autre) {
        if ((autre.largeur() != largeur()) || (autre.hauteur() != hauteur()))
            return false;
        for (int i = 0; i < hauteur(); i++)
            for (int j = 0; j < largeur(); j++)
                if (terrain[i][j] != autre.terrain[i][j])
                    return false;
        return true;
    }

    public boolean estSymetrique(Point origine) {
        return compterColonneLibre(origine) == compterLigneLibre(origine);
    }

    /**
     * compte le nombre de cases libres sur une colonne
     * 
     * @return nombre de case libres sur la colonne
     */
    public int compterColonneLibre(Point dep) {
        int nbCasesLibres = 0;
        for (int i = dep.y; i < hauteur && estMangeable(dep.x, i); i++) {
            nbCasesLibres++;
        }
        return nbCasesLibres;
    }

    /**
     * compte le nombre de cases libres sur une ligne
     * 
     * @return nombre de case libres sur la colonne
     */
    public int compterLigneLibre(Point dep) {
        int nbCasesLibres = 0;
        for (int i = dep.x; i < largeur && estMangeable(i, dep.y); i++) {
            nbCasesLibres++;
        }
        return nbCasesLibres;
    }
}
