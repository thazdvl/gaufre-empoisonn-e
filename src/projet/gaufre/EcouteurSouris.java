package projet.gaufre;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import Dessin.Point;

public class EcouteurSouris implements MouseListener {

    private Moteur              moteur;
    private TerrainGraphique    tgraph;

    public EcouteurSouris(Moteur m, TerrainGraphique tg) {
        super();
        moteur = m;
        tgraph = tg;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (moteur.aPerdu)
            return;
        
        int x = tgraph.calculeColonne(e.getX());
        int y = tgraph.calculeLigne(e.getY());
        System.out.println("Clic sur (" + x + "," + y + ")");
        synchronized (Moteur.lock) {
            if (moteur.setPoint(new Point(x, y)))
                Moteur.lock.notify();
        }
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
    }
}
