package projet.gaufre;

import Dessin.Point;

public interface Joueur {
    public Point jouerCoup();
}