package projet.gaufre;


import Dessin.Point;

public class Moteur {
    private Terrain      terrain;
    public static Object lock = new Object();
    
    private Point point;
    public boolean aPerdu = false;
    
    private Historique historique;
    
    public Moteur(Terrain t) {
        super();
        
        assert terrain != null;
        terrain = t;
        
        historique = new Historique();
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }
    
    public boolean setPoint(Point p) {
        if (!terrain.estMangeable(p.x, p.y))
            return false;
        point = p;
        return true;
    }
    
    public void executerCoup(Case joueur) {
        terrain.assignerZone(joueur, point);
        historique.ajouterNouveauCoup(point);
        if (point.x==0 && point.y==0) {
            aPerdu = true;
            actionPerdu(joueur);
        }
    }
    
    public int numJoueurCourant() {
    	return historique.getIt().nextIndex()%2;
    }
    
    public void annulerCoup() {
    	try {
            historique.annulerCoup();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    	historique.getListCoupJoues();
    	
    	terrain.efface();
    	for (int i=0 ; i<historique.getIt().nextIndex() ; i++)
    		terrain.assignerZone(i%2==0?Case.MANGE_J1:Case.MANGE_J2
    				, historique.getListCoupJoues().get(i));
    }
    
    public void refaireCoup() {
    	Point next = null;
        try {
            next = historique.refaireCoup();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    	if (next==null)
    		return;
    	terrain.assignerZone(historique.getIt().nextIndex()%2==0
    			?Case.MANGE_J2
    			:Case.MANGE_J1, next);
    }
    
    public void actionPerdu(Case joueur) {
        String message = "Le joueur "
                + (joueur==Case.MANGE_J1?1:2)
                +" a perdu.";
        System.out.println(message);
    }

}
