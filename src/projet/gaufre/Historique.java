package projet.gaufre;

import java.util.LinkedList;
import java.util.ListIterator;

import Dessin.Point;

public class Historique {
    private ListIterator<Point> it;
    private LinkedList<Point>   listCoupJoues;

    /* Constructeurs */

    public Historique(LinkedList<Point> listCoupJoues) {
        if (listCoupJoues == null) {
            this.listCoupJoues = new LinkedList<Point>();
        } else {
            this.listCoupJoues = listCoupJoues;
        }

        it = this.listCoupJoues.listIterator();
    }

    /**
     * Constructeur par défaut
     */
    public Historique() {
        this(null);
    }

    /* Work code */

    /**
     * Ajoute un nouveau coup dans l'historique. Supprime les coups qui ne sont plus jouable de l'historique.
     * 
     * @param p
     *        coordonnée du point qui a été joué.
     */
    public void ajouterNouveauCoup(Point p) {
        // TODO: mettre à jour l'ui si necessaire (griser bouton de l'ui, si nécessaire)
        if (p == null) {
            throw new IllegalArgumentException("le point à ajouter à l'historique ne peut être nulle");
        }
        if (p.x < 0 || p.y < 0) {
            throw new IllegalArgumentException("le point à ajouter à l'historique ne peut avoir de coordonnée négatif");
        }

        while (it.hasNext()) {
            it.next();
            it.remove();
        }
        it.add(p);
    }

    public void annulerCoup() throws Exception {
        if (it.hasPrevious())
            it.previous();
        else
            throw new Exception("impossible d'annuler les coups");
    }

    public Point refaireCoup() throws Exception {
        if (it.hasNext())
            return it.next();
        throw new Exception("Impossible de refaire le coup");
    }

    /* Getters et Setters */

    public ListIterator<Point> getIt() {
        return it;
    }

    public void setIt(ListIterator<Point> it) {
        this.it = it;
    }

    public LinkedList<Point> getListCoupJoues() {
        return listCoupJoues;
    }

    public void setListCoupJoues(LinkedList<Point> listCoupJoues) {
        this.listCoupJoues = listCoupJoues;
    }

}
