package projet.gaufre;

public class Arbitre implements Runnable {
    private Joueur joueur1, joueur2;
    private Moteur moteur;
    
    public Arbitre(Moteur m) {
        moteur = m;
    }
    
    public void setJoueur1(Joueur j) {
        joueur1 = j;
    }
    
    public void setJoueur2(Joueur j) {
        joueur2 = j;
    }
    
    private Joueur getJoueurCourant() {
    	return moteur.numJoueurCourant()==0?joueur1:joueur2;
    }
    
    private Case getCouleurCourant() {
    	return moteur.numJoueurCourant()==0?Case.MANGE_J1:Case.MANGE_J2;
    }
    
    @Override
    public void run() {
        while (!moteur.aPerdu) {
            if (getJoueurCourant() == null) {
                try {
                    synchronized (Moteur.lock) {
                        Moteur.lock.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                moteur.executerCoup(getCouleurCourant());
            } else {
                moteur.setPoint(getJoueurCourant().jouerCoup());
                moteur.executerCoup(getCouleurCourant());
            }
        }
    }
    
}
