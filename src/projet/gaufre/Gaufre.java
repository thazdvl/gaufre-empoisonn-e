package projet.gaufre;

import Dessin.*;

public class Gaufre {
    public static void main(Fenetre f, Evenements e, String [] args) {
        Terrain t = Terrain.defaut();

        //Joueur j1 = new NiveauUn(t);
        //Joueur j2 = new NiveauUn(t);
        //Joueur j1 = null;
        //Joueur j2 = new NiveauDeux(t);
        
        Joueur j1 = null;
        Joueur j2 = null;
        
        //Joueur j1 = new NiveauTrois(t);
        //Joueur j2 = new NiveauTrois(t);
        
        TerrainGraphique tg;
        Moteur m = new Moteur(t);
        
        f.setDrawAreaSize(50*t.largeur(),50*t.hauteur());
        tg = new TerrainGraphique(f, t);
        t.addObserver(tg);

        EcouteurSouris s = new EcouteurSouris(m, tg);
        EcouteurClavier c = new EcouteurClavier(m);
        e.addMouseListener(s);
        e.addKeyListener(c);
        
        Arbitre arbitre = new Arbitre(m);
        arbitre.setJoueur1(j1);
        arbitre.setJoueur2(j2);

        Dessin.Parameters.requiresOverdraw = false;
        f.tracerSansDelai(tg);

        Thread thread = new Thread(arbitre);
        thread.start();

        
        while (true) {
            e.waitForEvent();
        }
    }
}
