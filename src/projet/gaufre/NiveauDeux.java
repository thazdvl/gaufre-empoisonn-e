package projet.gaufre;

import java.util.LinkedList;
import java.util.Random;

import org.w3c.dom.ls.LSInput;

import Dessin.Point;

public class NiveauDeux implements Joueur {
    private Terrain terrain;

    public NiveauDeux(Terrain t) {
        terrain = t;
    }

    @Override
    public Point jouerCoup() {
        Random r = new Random();
        // Si la case (1,1) est jouable => stratégie gagnante
        if (terrain.consulter(1, 1).equals(Case.LIBRE)) {
            return new Point(1, 1);
        }

        // Sinon on joue de façon à toujours avoir la même quantité de case dans la première ligne/colonne
        int hauteurJouable = 0;
        for (int l = 1; l < terrain.hauteur() && terrain.estMangeable(0, l); l++) {
            hauteurJouable++;
        }
        int largeurJouable = 0;
        for (int c = 1; c < terrain.largeur() && terrain.estMangeable(c, 0); c++) {
            largeurJouable++;
        }
        if (largeurJouable > hauteurJouable) {
            return new Point(hauteurJouable + 1, 0);
        } else if (largeurJouable < hauteurJouable) {
            return new Point(0, largeurJouable + 1);
        } else if(largeurJouable ==0 && hauteurJouable==0) {
            return new Point(0,0);
        }
        int x = r.nextInt(largeurJouable)+1;
        int y = r.nextInt(hauteurJouable)+1;
        if(r.nextBoolean()) {
            return new Point(x, 0);
        } else {
            return new Point(0,y);
        }
        
        

    }
}
