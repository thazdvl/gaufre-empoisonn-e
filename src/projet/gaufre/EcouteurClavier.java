package projet.gaufre;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class EcouteurClavier implements KeyListener {
	private Moteur moteur;

	public EcouteurClavier(Moteur m) {
		moteur = m;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
	    switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			moteur.annulerCoup();
			break;
		case KeyEvent.VK_RIGHT:
			moteur.refaireCoup();
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}
}
