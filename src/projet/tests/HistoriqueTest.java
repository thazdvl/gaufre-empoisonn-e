package projet.tests;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Dessin.Point;
import projet.gaufre.Historique;

public class HistoriqueTest {
    protected Historique h;
    protected Point      pNeg, pNegX, pNegY, pUn, pZero;
    protected Random     r;

    @Before
    public void setUp() throws Exception {
        h = new Historique();
        this.r = new Random();
        pNeg = new Point(-1, -10);
        pNegX = new Point(-48, 75);
        pNegY = new Point(1, -9);

        pUn = new Point(1, 1);
        pZero = new Point(0, 0);

    }

    @After
    public void tearDown() throws Exception {
        h = null;
        r = null;
        pNeg = null;
        pNegX = null;
        pNegY = null;
        pUn = null;
        pZero = null;
    }

    /* Cas d'ajout d'un nouveau coup */
    @Test(expected = IllegalArgumentException.class)
    public void testAjouterNouveauCoupBloquantPointNull() {
        h.ajouterNouveauCoup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAjouterNouveauCoupBloquantPointNegatif() {
        h.ajouterNouveauCoup(pNeg);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAjouterNouveauCoupBloquantPointNegatifX() {
        h.ajouterNouveauCoup(pNegX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAjouterNouveauCoupBloquantPointNegatifY() {
        h.ajouterNouveauCoup(pNegY);
    }

    @Test
    public void testAjouterNouveauCoupSimpleZero() {
        h.ajouterNouveauCoup(pZero);
        assertEquals(h.getListCoupJoues().size(), 1);
        assertEquals(h.getListCoupJoues().getLast(), pZero);
    }

    @Test
    public void testAjouterNouveauCoupSimpleUn() {
        h.ajouterNouveauCoup(pUn);
        assertEquals(h.getListCoupJoues().size(), 1);
        assertEquals(h.getListCoupJoues().getLast(), pUn);
    }

    @Test
    public void testAjouterNouveauCoupSimpleRandom() {
        LinkedList<Point> l = new LinkedList<>();
        for (int i = 0; i < 1000; ++i) {
            Point p = new Point(r.nextInt(1000), r.nextInt(1000));
            h.ajouterNouveauCoup(p);
            l.add(p);
        }
        assertEquals(l.size(), h.getListCoupJoues().size());
        assertEquals(l, h.getListCoupJoues());

    }

    /* Cas un peu plus compliqué */

    // test d'un nouveau coup après un annulerCoup
    @Test
    public void testAjouterNouveauCoupSimpleApresAnnuler() throws Exception {

        /*
         * Note: Utilisation de random par paresse... Exécuter plusieurs fois le test pour vérifier qu'il n'y a pas de
         * biais !
         */
        Point p1 = new Point(r.nextInt(1000), r.nextInt(1000));
        Point p2 = new Point(r.nextInt(1000), r.nextInt(1000));
        Point p3 = new Point(r.nextInt(1000), r.nextInt(1000));
        Point p4 = new Point(r.nextInt(1000), r.nextInt(1000));

        h.ajouterNouveauCoup(p1);
        h.annulerCoup();

        assertEquals(h.getListCoupJoues().size(), 1);
        assertEquals(h.getListCoupJoues().get(0), p1);

        h.ajouterNouveauCoup(p2);

        assertEquals(h.getListCoupJoues().size(), 1);
        assertEquals(h.getListCoupJoues().get(0), p2);

        h.annulerCoup();

        assertEquals(h.getListCoupJoues().size(), 1);
        assertEquals(h.getListCoupJoues().get(0), p2);

        h.ajouterNouveauCoup(p3);
        h.ajouterNouveauCoup(p4);

        assertEquals(h.getListCoupJoues().size(), 2);
        assertEquals(h.getListCoupJoues().get(1), p4);
        assertEquals(h.getListCoupJoues().get(0), p3);
    }

    // test d'un nouveau coup après un refaireCoup
    @Test
    public void testAjouterNouveauCoupSimpleApresRefaire() throws Exception {

        Point p1 = new Point(r.nextInt(1000), r.nextInt(1000));
        Point p2 = new Point(r.nextInt(1000), r.nextInt(1000));
        Point p3 = new Point(r.nextInt(1000), r.nextInt(1000));
        Point p4 = new Point(r.nextInt(1000), r.nextInt(1000));

        Point p5 = new Point(r.nextInt(1000), r.nextInt(1000));
        Point p6 = new Point(r.nextInt(1000), r.nextInt(1000));

        h.ajouterNouveauCoup(p1);
        h.annulerCoup(); // position courante = fond de pile de l'historique

        h.ajouterNouveauCoup(p3); // position courante = p3
        assertEquals(h.getListCoupJoues().size(), 1);
        assertEquals(h.getListCoupJoues().get(0), p3);

        h.ajouterNouveauCoup(p2); // position courante = p2 (p2-->p3)
        h.annulerCoup(); // position courante = p3
        h.refaireCoup(); // position courante = p2 (p2-->p3)

        assertEquals(h.getListCoupJoues().size(), 2);
        assertEquals(h.getListCoupJoues().get(1), p2);
        assertEquals(h.getListCoupJoues().get(0), p3);

        h.ajouterNouveauCoup(p1); // position courante = p1 (p1-->p2-->p3)

        assertEquals(h.getListCoupJoues().size(), 3);
        assertEquals(h.getListCoupJoues().get(2), p1);
        assertEquals(h.getListCoupJoues().get(1), p2);
        assertEquals(h.getListCoupJoues().get(0), p3);

        h.ajouterNouveauCoup(p4); // postion courante = p4 (p4-->p1-->p2-->p3)
        h.ajouterNouveauCoup(p5); // postion courante = p5 (p5-->p4-->p1-->p2-->p3)
        h.ajouterNouveauCoup(p6); // postion courante = p6 (p6-->p5-->p4-->p1-->p2-->p3)

        assertEquals(h.getListCoupJoues().size(), 6);
        assertEquals(h.getListCoupJoues().get(5), p6);
        assertEquals(h.getListCoupJoues().get(4), p5);
        assertEquals(h.getListCoupJoues().get(3), p4);
        assertEquals(h.getListCoupJoues().get(2), p1);
        assertEquals(h.getListCoupJoues().get(1), p2);
        assertEquals(h.getListCoupJoues().get(0), p3);

        h.annulerCoup();// postion courante = p5 (p5-->p4-->p1-->p2-->p3)
        h.annulerCoup();// postion courante = p4 (p4-->p1-->p2-->p3)
        h.annulerCoup();// postion courante = p1 (p1-->p2-->p3)

        assertEquals(h.getListCoupJoues().size(), 6);
        assertEquals(h.getListCoupJoues().get(5), p6);
        assertEquals(h.getListCoupJoues().get(4), p5);
        assertEquals(h.getListCoupJoues().get(3), p4);
        assertEquals(h.getListCoupJoues().get(2), p1);
        assertEquals(h.getListCoupJoues().get(1), p2);
        assertEquals(h.getListCoupJoues().get(0), p3);

        h.refaireCoup();// postion courante = p4 (p4-->p1--
        assertEquals(h.getListCoupJoues().size(), 6);
        assertEquals(h.getListCoupJoues().get(5), p6);
        assertEquals(h.getListCoupJoues().get(4), p5);
        assertEquals(h.getListCoupJoues().get(3), p4);
        assertEquals(h.getListCoupJoues().get(2), p1);
        assertEquals(h.getListCoupJoues().get(1), p2);
        assertEquals(h.getListCoupJoues().get(0), p3);

        h.refaireCoup();// postion courante = p5 (p5-->p4-->p1-->p2-->p3)

        assertEquals(h.getListCoupJoues().size(), 6);
        assertEquals(h.getListCoupJoues().get(5), p6);
        assertEquals(h.getListCoupJoues().get(4), p5);
        assertEquals(h.getListCoupJoues().get(3), p4);
        assertEquals(h.getListCoupJoues().get(2), p1);
        assertEquals(h.getListCoupJoues().get(1), p2);
        assertEquals(h.getListCoupJoues().get(0), p3);

        h.ajouterNouveauCoup(p2);// postion courante = p2 (p2-->p5-->p4-->p1-->p2-->p3)

        assertEquals(h.getListCoupJoues().size(), 6);
        assertEquals(h.getListCoupJoues().get(5), p2);
        assertEquals(h.getListCoupJoues().get(4), p5);
        assertEquals(h.getListCoupJoues().get(3), p4);
        assertEquals(h.getListCoupJoues().get(2), p1);
        assertEquals(h.getListCoupJoues().get(1), p2);
        assertEquals(h.getListCoupJoues().get(0), p3);
    }

    @Test(expected = Exception.class)
    public void testAnnulerCoupBloquantVide() throws Exception {

        h.annulerCoup();
    }

    @Test(expected = Exception.class)
    public void testAnnulerCoupBloquantPlusDeCoup() throws Exception {
        try {
            Point p1 = new Point(r.nextInt(1000), r.nextInt(1000));
            Point p2 = new Point(r.nextInt(1000), r.nextInt(1000));
            h.ajouterNouveauCoup(p1);
            h.ajouterNouveauCoup(p2);
            h.annulerCoup();
            h.annulerCoup();
        } catch (Exception e) {
            fail("Erreur inattendue !");
        }
        h.annulerCoup();
    }

    @Test(expected = Exception.class)
    public void testRefaireCoupBloquantVide() throws Exception {
        h.refaireCoup();
    }

    @Test(expected = Exception.class)
    public void testRefaireCoupBloquantPlusDeCoup() throws Exception {
        try {
            Point p1 = new Point(r.nextInt(1000), r.nextInt(1000));
            Point p2 = new Point(r.nextInt(1000), r.nextInt(1000));
            h.ajouterNouveauCoup(p1);
            h.ajouterNouveauCoup(p2);
            h.annulerCoup();
            h.annulerCoup();
            h.refaireCoup();
            h.refaireCoup();
        } catch (Exception e) {
            fail("Erreur inattendue !");
        }
        h.refaireCoup();
    }

}
