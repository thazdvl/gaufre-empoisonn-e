package projet.tests;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Dessin.Point;
import projet.gaufre.*;

public class MoteurTest {
	protected Terrain terrain;
	protected int numJoueur;
	protected Joueur[] joueurs;
	protected Moteur m;
	protected Point      pNeg, pNegX, pNegY, pUn, pZero;
    protected Random     r;Joueur j1, j2;

    @Before
    public void setUp() throws Exception {
    	
    	joueurs = new Joueur [2];
    	joueurs[0] = j1;
        joueurs[1] = j2;
        m = new Moteur(terrain , j1, j2);
        this.r = new Random();
        pNeg = new Point(-1, -10);
        pNegX = new Point(-48, 75);
        pNegY = new Point(1, -9);

        pUn = new Point(1, 1);
        pZero = new Point(0, 0);

    }

    @After
    public void tearDown() throws Exception {
        m = null;
        r = null;
        terrain = null;
        pNeg = null;
        pNegX = null;
        pNegY = null;
        pUn = null;
        pZero = null;
    }
	
	@Test
	public void testNextJoueur() throws Exception {
		int num= m.getNumeroJoueurCourant();
		m.changerJoueur();
		assertEquals(num, m.getNumeroJoueurCourant());	
	}
	
	 @Test(expected = IllegalArgumentException.class)
	    public void testEliminerCasePointNull() {
	        m.eliminerCase(null);
	    }

	    @Test(expected = IllegalArgumentException.class)
	    public void testEliminerCasePointNegatif() {
	       m.eliminerCase(pNeg);
	    }

	    @Test(expected = IllegalArgumentException.class)
	    public void testEliminerCasePointNegatifX() {
	        m.eliminerCase(pNegX);
	    }

	    @Test(expected = IllegalArgumentException.class)
	    public void testEliminerCasePointNegatifY() {
	    	m.eliminerCase(pNegY);
	    }
	    
	    @Test(expected = Exception.class)
	    public void testNbrCasesApresElimine() throws Exception {
	    	int num = terrain.largeur() * terrain.hauteur();
	    	Point p = new Point(r.nextInt(1000), r.nextInt(1000));
	    	m.eliminerCase(p);
	    	int res = terrain.largeur() * terrain.hauteur();
	    	
	    	assertEquals(num, res);
	    }
	    
	    @Test(expected = Exception.class)
	    public void testZonesAssignees() throws Exception {
	    	Point p = new Point(r.nextInt(1000), r.nextInt(1000));
	    	try {
		    	
		    	m.eliminerCase(p);
	    	} catch (Exception e){
		    	for (int i = p.x; i < terrain.largeur(); i++) {
		            for (int j = p.y; j < terrain.hauteur(); j++) {
		            	if((!terrain.estMangeable(i, j)))
		            		fail("Erreur inattendue !");		            		
		            }
		    	}
	    	}
	    		m.eliminerCase(p);
	    }
}
